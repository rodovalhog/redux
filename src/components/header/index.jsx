import { useMemo, useState } from "react";

// Components
import Cart from "../cart/index";

// Styles
import * as Styles from "./styles";
import { useSelector, useDispatch } from "react-redux";
import { loginUser, logoutUser } from "../../redux/user/actions";


function Header() {
  const [cartIsVisible, setCartIsVisible] = useState(false);
  const { userReducer, cartReducer } = useSelector(rootReducer => rootReducer)
  const {currentUser} = userReducer
  const {products} = cartReducer
  const dispach = useDispatch()

  const productCount = useMemo(() => {
return products.reduce((acc, curr) => acc + curr.quantity, 0)
  }, [products])

  const handleLoginClick = () => {
    dispach(loginUser({
      nome: 'Guilherme',
      email: 'rodovalhog@gmail.com'
    }))
  }

  const handleLogoutClick = () => {
    dispach(logoutUser())
  }

  const handleCartClick = () => {
    setCartIsVisible(true);
  };

  return (
    <Styles.Container>
      <Styles.Logo>Redux Shopping</Styles.Logo>
      <Styles.Buttons>
      {currentUser ? <div onClick={handleLogoutClick}>Logout</div> :  <div onClick={handleLoginClick}>Login</div> }
        <div onClick={handleCartClick}>Carrinho <span>({productCount})</span></div>
      </Styles.Buttons>
      <Cart isVisible={cartIsVisible} setIsVisible={setCartIsVisible} />
    </Styles.Container>
  );
}

export default Header;
