import { UserActionTypes } from "./action-type";

const intialState = {
    currentUser: null,
}

const userReducer = (state = intialState, action) => {
    switch(action.type) {
        case UserActionTypes.LOGIN:
            return {
                ...state,
                currentUser: action.payload
            };
            case UserActionTypes.LOGOUT:
                return {
                    ...state,
                    currentUser: null
                }
                default:
            return state
    }
};

export default userReducer;