
import { CartActionType } from "./action-type";


const intialState = {
    products: [],
    productTotalPrices: 0
}

const cartReducer = (state = intialState, action) => {

    switch(action.type) {
        case CartActionType.ADD_PRODUCT:
            const isProductAlredyInCard = state.products.some(product => product.id === action.payload.id)
            if(isProductAlredyInCard) {
                return {
                    ...state,
                    products: state.products.map(product => {
                        return product.id === action.payload.id 
                        ? { ...product, quantity: product.quantity +1}
                        : product
                    }) 
                }
            }

            return {
                ...state,
                products: [...state.products, {...action.payload, quantity: 1}]
            };

            case CartActionType.INCREASE_QUANTITY:
                const increseItem = state.products.map(product => {

                    if(product.id === action.payload.id) {
                        return {
                            ...product,
                            quantity: product.quantity + 1
                        }
                    }
                return product
                }).filter(product => product.quantity > 0)
                return {
                    ...state, 
                    products: [...increseItem]
                };

                case CartActionType.DECREASE_QUANTITY:

                    const decreseItem = state.products.map(product => {
                        if(product.id === action.payload.id ) {
                    
                            return {
                                ...product,
                                quantity: product.quantity - 1
                            }
                        }
                    return product
                    }).filter(product => product.quantity > 0)
                    return {
                        ...state, 
                        products: [...decreseItem]
                    };
                    
                
            case CartActionType.REMOVE_PRODUCT:
                return {
                    ...state,
                    products: state.products.filter(item => item.id !== action.payload.id)
                }
                default:
            return state
    }
};

export default cartReducer;