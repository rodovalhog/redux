export const CartActionType = {
    ADD_PRODUCT: 'cart/addProduct',
    REMOVE_PRODUCT: 'remove/card',
    INCREASE_QUANTITY: 'increse/quantity',
    DECREASE_QUANTITY: 'decrese/quantity'

}