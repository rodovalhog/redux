import { CartActionType } from "./action-type";

export const addProductToCart = (payload) => ({
    type: CartActionType.ADD_PRODUCT,
    payload
})

export const removeProductToCart = (payload) => ({
    type: CartActionType.REMOVE_PRODUCT,
    payload
})

export const incraseQuantityProductToCart = (payload) => ({
    type: CartActionType.INCREASE_QUANTITY,
    payload
})

export const decraseQuantityProductToCart= (payload) => ({
    type: CartActionType.DECREASE_QUANTITY,
    payload
})