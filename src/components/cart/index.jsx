// Styles
import { useSelector } from "react-redux";
import * as Styles from "./styles";
import CartItem from '../cart-item'

const Cart = ({ isVisible, setIsVisible }) => {
  const { products } = useSelector(reducers => reducers.cartReducer)
  console.log("products", products)


  const handleEscapeAreaClick = () => setIsVisible(false);

  return (
    <Styles.CartContainer isVisible={isVisible}>
      <Styles.CartEscapeArea onClick={handleEscapeAreaClick} />
      <Styles.CartContent>
        <Styles.CartTitle>Seu Carrinho</Styles.CartTitle>
        {products.map((product, index) => {
          return <CartItem key={index} product={product}/>
        })}
      </Styles.CartContent>
    </Styles.CartContainer>
  );
};

export default Cart;
